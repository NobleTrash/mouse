<?php

/**
 * Class ExampleAbstractClass
 */
class ExampleAbstractClass
{
    public $name = 'Abstract class';

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
}
