<?php

include ('ExampleAbstractClass.php');
include ('ExampleTrait.php');
include ('ExampleInterface.php');
/**
 * Class ExampleClass
 */
class ExampleClass extends ExampleAbstractClass implements ExampleInterface
{
    use ExampleTrait;

    public $description = 'I\'m example class';

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }
}
