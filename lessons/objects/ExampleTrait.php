<?php

/**
 * Trait ExampleTrait
 */
trait ExampleTrait
{
    /**
     * @param string $name
     * @param string $description
     * @return string
     */
    public function getFullDescription($name, $description)
    {
        return $name . ' ' . $description;
    }
}
